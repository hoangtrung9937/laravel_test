<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\usern;
class handleform extends Controller
{
    public function index()
    {
//        $data = DB::table('users')->get();
        $data = usern::all();
        return view('viewuser',compact('data'));
    }
    public function getForm(){
        return view('FormRequest');
    }

    public function handleForm(Request $request)
    {
        $username = $request->username;
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phonenumber;
        $address = $request->address;
        $birthday = $request->birthday;
        $pass = $request->password;
//        DB::table('users')->insert(
//            [
//                'username' => $username,
//                'name' => $name,
//                'email' => $email,
//                'phone' => $phone,
//                'address' => $address,
//                'birthday' => $birthday,
//                'password' => $pass
//            ]
//        );
        $newUser = new usern();
        $newUser->username = $username;
        $newUser->name = $name;
        $newUser->email = $email;
        $newUser->phone = $phone;
        $newUser->address = $address;
        $newUser->birthday = $birthday;
        $newUser->password = $pass;
        $newUser->save();
        return redirect('list-user');
    }

    public function deleteUser(int $id)
    {
        usern::destroy($id);
//        usern::where('id',$id)->delete();
        return redirect('list-user');
    }
    public function editUserForm(int $id)
    {
        $us=usern::find($id);
        return view('EditUser',compact('us'));
    }
    public function editUser(Request $request)
    {
        $username = $request->username;
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phonenumber;
        $address = $request->address;
        $birthday = $request->birthday;
        $pass = $request->password;

        $newUser = usern::find($request->id);
        $newUser->username = $username;
        $newUser->name = $name;
        $newUser->email = $email;
        $newUser->phone = $phone;
        $newUser->address = $address;
        $newUser->birthday = $birthday;
        $newUser->password = $pass;
        $newUser->save();
        return redirect('list-user');
    }
}
