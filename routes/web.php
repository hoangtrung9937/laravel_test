<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/about', function () {
    return view('About');
});
Route::get('/', function () {
    return view('welcome');
});
Route::get('call-about/{name}/{age}', 'about@index');
Route::get('get-form','handleform@getForm');
Route::post('handle-form', 'handleform@handleForm');
Route::get('list-user', 'handleform@index');
Route::get('del-user/{id}', 'handleform@deleteUser');
Route::get('edit-user/{id}', 'handleform@editUserForm');
Route::post('editUser-form', 'handleform@editUser');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('vue', function (){
    return view('vue');
});
