<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Test form</title>
    <link rel="stylesheet" href="">
    <!-- Latest compiled and minified CSS & JS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
    <script src="//code.jquery.com/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" ></script>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('list-user')}}">Back to list</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Link</a></li>
                <li><a href="#">Link</a></li>
            </ul>
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Link</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>
<div class="container">
    <form action="{{ url('editUser-form') }}" method="POST" role="form" class="form-horizontal" enctype="multipart/form-data">
        <legend>Đăng kí người dùng Laravel</legend>
        {{ csrf_field()}}
        <input type="hidden" name="id" value="{{$us->id}}">
        <div class="form-group clearfix row">
            <div class="col-md-3 float-left">
                <span class="control-label">Username</span>
            </div>
            <div class="col-md-9 float-left">
                <input type="text" name="username" id="inputUsername" class="form-control" value="{{$us->username}}">
            </div>
        </div>
        <div class="form-group clearfix row">
            <div class="col-md-3 float-left">
                <span class="control-label">Name</span>
            </div>
            <div class="col-md-9 float-left">
                <input type="text" name="name" id="inputname" class="form-control" value="{{$us->name}}">
            </div>
        </div>
        <div class="form-group clearfix row">
            <div class="col-md-3 float-left">
                <span class="control-label">Email</span>
            </div>
            <div class="col-md-9 float-left">
                <input type="text" name="email" id="inputemail" class="form-control" value="{{$us->email}}">
            </div>
        </div>
        <div class="form-group clearfix row">
            <div class="col-md-3 float-left">
                <span class="control-label">Phone</span>
            </div>
            <div class="col-md-9 float-left">
                <input type="text" name="phonenumber" id="inputphonenumber" class="form-control" value="{{$us->phone}}">
            </div>
        </div>
        <!--     <div class="form-group clearfix row">
                    <div class="col-md-3 float-left">
                        <span class="control-label">Image</span>
                    </div>
                    <div class="col-md-9 float-left">
                        <input type="file" name="image" id="inputimage" class="form-control">
                    </div>
            </div> -->
        <div class="form-group clearfix row">
            <div class="col-md-3 float-left">
                <span class="control-label">Address</span>
            </div>
            <div class="col-md-9 float-left">
                <input type="text" name="address" id="inputaddress" class="form-control" value="{{$us->address}}">
            </div>
        </div>
        <div class="form-group clearfix row">
            <div class="col-md-3 float-left">
                <span class="control-label">Birthday</span>
            </div>
            <div class="col-md-9 float-left">
                <input type="date" name="birthday" id="inputbirthday" class="form-control" value="{{$us->birthday}}">
            </div>
        </div>
        <div class="form-group clearfix row">
            <div class="col-md-3 float-left">
                <span class="control-label">Password</span>
            </div>
            <div class="col-md-9 float-left">
                <input type="text" name="password" id="inputpassword" class="form-control" value="{{$us->password}}">
            </div>
        </div>
        <div class="clearfix text-center"><button type="submit" class="btn btn-primary">Submit</button></div>
    </form>
</div>
</body>
</html>
